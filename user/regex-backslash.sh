#!/usr/bin/env bash
# Desc: Backslashes regex special characters
# Usage: cat file.txt | regex-backslash.sh
# Version: 0.0.1
# Depends: sed (GNU sed) 4.8; GNU bash, version 5.1.16(1)-release (x86_64-pc-linux-gnu)
# Ref/Attrib: [1] TIL: Reading stdin to a BASH variable https://dev.to/jeremyckahn/til-reading-stdin-to-a-bash-variable-1kln

STD_IN=$(</dev/stdin);

printf "%s\n" "$STD_IN" | \
    sed \
        -e 's/\\/\\\\/g'  \
        -e 's/\./\\./g'   \
        -e 's/\+/\\+/g'   \
        -e 's/\*/\\*/g'   \
        -e 's/\?/\\?/g'   \
        -e 's/\^/\\^/g'   \
        -e 's/\$/\\$/g'   \
        -e 's/(/\\(/g'    \
        -e 's/)/\\)/g'    \
        -e 's/\[/\\[/g'   \
        -e 's/\]/\\]/g'   \
        -e 's/{/\\{/g'    \
        -e 's/}/\\}/g'    \
        -e 's/|/\\|/g'    \
        -- -;

exit 0;

# Author: Steven Baltakatei Sandoval
# License: GPLv3+
