#!/usr/bin/env bash
# Desc: Prints mediawiki code for a journal month in the bk4 wiki.
# Usage: /bin/bash mw_create_month_journal.sh [int year] [int month]
# Input: arg1: year
#        arg2: month
# Example: /bin/bash mw_create_month_journal.sh 2023 03 > output.txt
# Version: 0.1.0

yyyy="$1";
mm="$2";

yell() { echo "$0: $*" >&2; } # print script path and all args to stderr
die() { yell "$*"; exit 111; } # same as yell() but non-zero exit status
must() { "$@" || die "cannot $*"; } # runs args as command, reports args if command fails

# Check input
## Check arg count
if [[ $# -ne 2 ]]; then die "FATAL:Invalid arg count:$#"; fi;
## Strip leading zeros (because August/octal)
yyyy="$((10#${yyyy}))";
mm="$((10#${mm}))";
## Check year
re="[0-9]+";
if [[ ! $yyyy =~ $re ]]; then die "FATAL:Invalid year string:$yyyy"; fi;
if [[ ! $yyyy -ge 1582 ]]; then die "FATAL:Invalid year string:$yyyy"; fi;
## Check month
if [[ $mm -lt 1 ]] || [[ $mm -gt 12 ]]; then die "FATAL:Invalid month:$mm"; fi;


# Calc working vars
## Years
if [[ $mm -gt 1 ]]; then
    yyyy_prev="$yyyy";
else    
    yyyy_prev="$((yyyy - 1))";
fi;
if [[ $mm -lt 12 ]]; then
    yyyy_next="$yyyy";
else
    yyyy_next="$((yyyy + 1))";
fi;
## Months
mm_prev="$((mm - 1))";
mm_next="$((mm + 1))";
if [[ $mm_prev -lt 1 ]]; then mm_prev="$((mm_prev + 12))"; fi;
if [[ $mm_next -gt 12 ]]; then mm_next="$((mm_next - 12))"; fi;
## Leap Years
if [[ ! $((yyyy % 4)) -eq 0 ]]; then
    ### Year is not evenly divisible by 4.
    leapyear="no";
elif [[ ! $((yyyy % 100)) -eq 0 ]]; then
    ### Year is not evenly divisible by 100.
    leapyear="yes";
elif [[ ! $((yyyy % 400)) -eq 0 ]]; then
    ### year is not evenly divisible by 400.
    leapyear="no";
else
    ### year is divisible by 4, 100, and 400
    leapyear="yes";
fi;

# Form and print header
printf "{{bk journal header month}}\n";
printf "Journal for [[%d]]-%02d. " "$yyyy" "$mm";
printf "Preceded by [[%d-%02d]]. " "$yyyy_prev" "$mm_prev";
printf "Followed by [[%d-%02d]]. " "$yyyy_next" "$mm_next";
printf "\n\n";

printf "==Events==\n";
printf "\n";

printf "==Tasks==\n";
printf "\n";

# Form and print body
for (( dd=1; dd <= 31; dd++ )); do
    if [[ $mm -eq 2 ]] && [[ $dd -eq 29 ]] && \
           [[ $leapyear == "no" ]]; then break; fi;
    if [[ $mm -eq 2 ]] && [[ $dd -ge 30 ]]; then break; fi;
    if [[ $mm -eq 4 ]] && [[ $dd -ge 31 ]]; then break; fi;
    if [[ $mm -eq 6 ]] && [[ $dd -ge 31 ]]; then break; fi;
    if [[ $mm -eq 9 ]] && [[ $dd -ge 31 ]]; then break; fi;
    if [[ $mm -eq 11 ]] && [[ $dd -ge 31 ]]; then break; fi;
    printf "==%4d-%02d-%02d==\n" "$yyyy" "$mm" "$dd";
    fs_iso8601_alt="+%G-W%V-%u, %Y-%j, %a"; # alternate ISO-8601 dates
    date_alt="$(date --date="$yyyy-$mm-$dd" "$fs_iso8601_alt")";
    printf "%s\n" "$date_alt";
    printf "\n";
done;
printf "\n";

# Form and print footer
s1="==References==";
s2="<references>";
s3="</references>";
printf "%s\n%s\n%s\n" "$s1" "$s2" "$s3";
printf "\n";

printf "==See Also==\n";
printf "\n";

printf "==Ext. Links==\n";
printf "\n";

printf "{{bk journal footer month}}\n";

s1="[[Category:Journals by month]]";
printf "%s\n" "$s1";
printf "\n";
