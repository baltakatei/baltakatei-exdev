#!/usr/bin/env bash
# Desc: Wrapper for creating yearly journal page generation script
# Input: arg1: path mw_create_year_journal.sh
#        arg2: year start
#        arg3: year end
# Example: mw_create_year_journal_range.sh ./mw_create_year_journal.sh 2030 2040
# Depends: BK-2020-03: mw_create_year_journal.sh
# Version: 0.0.2

# plumbing
path_cyj="$1"; # eg "$HOME/scripts/mw_create_year_journal.sh"
year_start="$2"; # eg 2030
year_end="$3"; # eg 2040
dir_out="./wikicode/";

yell() { echo "$0: $*" >&2; } # print script path and all args to stderr
die() { yell "$*"; exit 111; } # same as yell() but non-zero exit status
must() { "$@" || die "cannot $*"; } # runs args as command, reports args if command fails
check_depends() {
    # check location of `mw_create_year_journal.sh`
    if [[ ! -f "$path_cyj" ]]; then die "FATAL:Not found:$path_cyj"; fi;
};
check_plumbing() {
    if [[ ! -d "$dir_out" ]]; then
        yell "STATUS:Creating missing dir_out:$dir_out";
        mkdir -p "$dir_out"; fi;
};
main() {
    check_depends;
    check_plumbing;

    while read -r year; do
        cyj_args+=("$year");
        
        # Define output file path
        path_out="$dir_out"/"$(printf "%d" "$year")";
        
        # Execute
        "$path_cyj" "${cyj_args[@]}" > "$path_out";
        
        unset cyj_args;
    done < <(seq "$year_start" "$year_end"); # each year
};

main "$@";

# Author: Steven Baltakatei Sandoval
# License: GPLv3+
