#!/bin/bash
# Desc: Zero-pad working dir files with initial digits for sorting
# Usage: zeropad.sh [str fext]
# Example: zeropad.sh;
#          zeropad.sh ".jpg";
# Input: arg1  str  file extension of files to apply changes to
# Version: 0.2.0

declare fext; # file extension

list_files() {
    # Desc: Lists working directory file basenames
    # Usage: list_files
    # Input: fext  var  file extension
    # Output: stdout  a line-delimited list of file names
    if [[ -z "$fext" ]]; then
        find . -mindepth 1 -maxdepth 1 -type f -exec basename '{}' \; ;
    else
        find . -mindepth 1 -maxdepth 1 -type f -exec basename '{}' \; \
            | grep -E -- "${fext}$" ;
    fi;
}; # list pwd file basenames
remove_leading_zeroes() {
    # Desc: Removes leading zeroes from lines
    # Input: stdin
    # Output: stdout
    # Depends: BK-2020-03 read_stdin()
    # Version: 0.0.1
    while read -r line; do
        printf "%s\n" "$line" | sed -E -e 's/(^0*)([0-9].*)/\2/';
    done;
};
get_max_digits() {
    # Desc: Returns max leading digits of pwd filenames
    # Depends: list_files()
    #          BK-2020-03:  remove_leading_zeroes()
    #          GNU sed 4.8
    #          GNU awk 5.1.0
    local output;
    
    # Find the maximum number of leading digits in the filenames of working dir
    output="$(list_files | remove_leading_zeroes \
        | awk '{ match($0, /^[0-9]+/); if (RLENGTH > max) max=RLENGTH } END { print max }' )";
    # declare -p max_digits; # debug
    printf "%s" "$output";
}; # return max digits to stdout
main () {
    # Read file extension if provided
    if [[ -n "$1" ]]; then
        fext="$1";
    fi;

    max_digits="$(get_max_digits)";

    # Loop over the files and rename them
    while read -r file; do
        # Skip files with no leading digits
        re='^[0-9]+';
        if [[ ! "$file" =~ $re ]]; then continue; fi;

        # Extract the leading digits
        digits="$(echo "$file" | sed -E 's/([0-9]*).*/\1/')"; # all leading digits
        digits_nlz="$(echo "$digits" | remove_leading_zeroes)"; # no leading zeroes
        # Zero-pad the digits
        padded_digits="$(printf "%0${max_digits}d" "$digits_nlz")";
        # Construct the new filename
        new_file="${padded_digits}${file#${digits}}";
        # Rename the file
        if [[ "$file" == "$new_file" ]]; then continue; fi;
        mv -n "$file" "$new_file";
        #declare -p max_digits file digits digits_nlz padded_digits new_file # debug
    done < <(list_files);
}; # main program

main "$@";

# Author: Steven Baltakatei Sandoval
# License: GPLv3+
