(defun remove-single-newlines ()
  ;; Desc: Replace all single newlines with spaces.
  ;; Date: 2024-02-24
  "Remove single newlines, joining lines that are separated by a single newline, while preserving paragraphs separated by multiple newlines."
  (interactive)
  (let ((start (point-min)) (end (point-max)))
    (save-excursion
      (goto-char start)
      (while (re-search-forward "\\([^\n]\\)\n\\([^\n]\\)" end t)
        ;; Check if the newline is followed by another newline (indicating a paragraph break)
        (unless (looking-at-p "\n")
          (replace-match "\\1 \\2" nil nil))))))

(defun remove-single-newlines-region (start end)
  ;; Desc: Replace all single newlines in a selected region with spaces.
  ;; Date: 2024-02-24
  "Remove single newlines in the selected region, joining lines separated by a single newline, while preserving paragraphs separated by multiple newlines."
  (interactive "r") ; "r" means this function should use the region start and end as arguments when called interactively
  (save-excursion
    (goto-char start)
    (while (re-search-forward "\\([^\n]\\)\n\\([^\n]\\)" end t)
      ;; Check if the newline is followed by another newline (indicating a paragraph break)
      (unless (looking-at-p "\n")
        (replace-match "\\1 \\2" nil nil)))))
