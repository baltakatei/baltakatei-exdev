#!/usr/bin/env bash
# Desc: Wrapper for creating monthly journal page generation script
# Input: arg1: path mw_create_month_journal.sh
#        arg2: year start
#        arg3: year end
# Example: mw_create_month_journal_range.sh ./mw_create_month_journal.sh 2030 2040
# Depends: BK-2020-03: mw_create_month_journal.sh
# Version: 1.0.0

# plumbing
path_cmj="$1"; # eg "$HOME/scripts/mw_create_month_journal.sh"
year_start="$2"; # eg 2030
year_end="$3"; # eg 2040
dir_out="./wikicode/";

yell() { echo "$0: $*" >&2; } # print script path and all args to stderr
die() { yell "$*"; exit 111; } # same as yell() but non-zero exit status
must() { "$@" || die "cannot $*"; } # runs args as command, reports args if command fails
check_depends() {
    # check location of `mw_create_month_journal.sh`
    if [[ ! -f "$path_cmj" ]]; then die "FATAL:Not found:$path_cmj"; fi;
};
check_plumbing() {
    if [[ ! -d "$dir_out" ]]; then
        yell "STATUS:Creating missing dir_out:$dir_out";
        mkdir -p "$dir_out"; fi;
};
main() {
    check_depends;
    check_plumbing;

    while read -r year; do
        while read -r month; do
            monthp="$(printf "%2d" "$month")";
            # Define and execute create monthly journal command
            cmj_args+=("$year");
            cmj_args+=("$month");

            # Define output file path
            path_out="$dir_out"/"$(printf "%d-%02d" "$year" "$month")";

            # Execute
            "$path_cmj" "${cmj_args[@]}" > "$path_out";

            unset cmj_args;
        done < <(seq 1 12); # each month
    done < <(seq "$year_start" "$year_end"); # each year
};

main "$@";

# Author: Steven Baltakatei Sandoval
# License: GPlv3+
