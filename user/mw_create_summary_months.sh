#!/usr/bin/env bash
# Desc: Prints mediawiki code for a summary of months of a year on the bk4 wiki.
# Usage: /bin/bash mw_create_summary_months.sh [int year]
# Input: arg1: year
# Example: /bin/bash mw_create_summary_months.sh [int year] > output.txt
# Version: 0.0.1

yell() { echo "$0: $*" >&2; } # print script path and all args to stderr
die() { yell "$*"; exit 111; } # same as yell() but non-zero exit status
must() { "$@" || die "cannot $*"; } # runs args as command, reports args if command fails
main() {
    yyyy="$1";
    
    # Check input
    if [[ $# -ne 1 ]]; then die "FATAL:Invalid arg count:$#"; fi;
    ## Strip leading zeroes (because August/octal)
    yyyy="$((10#${yyyy}))";
    ## Check year
    re="[0-9]+";
    if [[ ! $yyyy =~ $re ]]; then die "FATAL:Invalid year string:$yyyy"; fi;
    if [[ ! $yyyy -ge 1582 ]]; then die "FATAL:Invalid year string:$yyyy"; fi;

    # Calc working vars
    ## Years
    yyyy_prev="$((yyyy - 1))";
    yyyy_next="$((yyyy + 1))";

    # Form and print header
    printf "{{bk journal header}}\n";
    printf "A summary of monthly journals of [[%d]]. " "$yyyy";
    printf "Preceded by [[Summary of months of %d]]. " "$yyyy_prev";
    printf "Followed by [[Summary of months of %d]]. " "$yyyy_next";
    printf "\n\n";

    # Form and print body
    for (( mm=1; mm <= 12; mm++)); do
        printf "=[[%4d-%02d]]=" "$yyyy" "$mm";
        printf "\n";
        printf "{{:%4d-%02d}}" "$yyyy" "$mm";
        printf "\n";
        
    done;
    printf "\n\n";

    # Form and print footer
    printf "{{bk journal footer}}\n";
    s1="[[Category:Monthly summaries by year]]"
    printf "%s\n" "$s1";
    printf "\n";
    
}; # main program

main "$@";

# Author: Steven Baltakatei Sandoval
#+License: GPLv3+
