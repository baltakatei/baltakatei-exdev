#!/usr/bin/env bash
# Desc: Prints mediawiki code for a journal year in the bk4 wiki.
# Usage: /bin/bash mw_create_year_journal.sh [int year]
# Input: arg1: year
# Example: /bin/bash mw_create_year_journal.sh 2023 > output.txt
# Version: 0.1.0

yyyy="$1";

yell() { echo "$0: $*" >&2; } # print script path and all args to stderr
die() { yell "$*"; exit 111; } # same as yell() but non-zero exit status
must() { "$@" || die "cannot $*"; } # runs args as command, reports args if command fails

# Check input
## Check arg count
if [[ $# -ne 1 ]]; then die "FATAL:Invalid arg count:$#"; fi;
## Strip leading zeros
yyyy="$((10#${yyyy}))";

## Check year
re="[0-9]+";
if [[ ! $yyyy =~ $re ]]; then die "FATAL:Invalid year string:$yyyy"; fi;
if [[ ! $yyyy -ge 1582 ]]; then die "FATAL:Invalid year string:$yyyy"; fi;

# Calc working vars
## Years
yyyy_prev="$((yyyy - 1))";
yyyy_next="$((yyyy + 1))";

# Form and print header
printf "{{bk journal header year}}\n";
printf "[[Journal]] for the year [[%d]]. " "$yyyy";
printf "Preceded by [[%d]]. " "$yyyy_prev";
printf "Followed by [[%d]]. " "$yyyy_next";
printf "\n\n";

printf "==Events==\n";
printf "\n";

# Form and print body
for (( mm=1; mm <= 12; mm++ )); do
    printf "==[[%4d-%02d]]==\n" "$yyyy" "$mm";
done;
printf "\n";

# Form and print footer
s1="==References==";
s2="<references>";
s3="</references>";
printf "%s\n%s\n%s\n" "$s1" "$s2" "$s3";
printf "\n";

printf "==See Also==\n";
printf "* [[Summary of months of {{PAGENAME}}]]\n"
printf "\n";

printf "==Ext. Links==\n";
printf "\n";

printf "{{bk journal footer year}}\n";

s1="[[Category:Journals by year]]";
printf "%s\n" "$s1";
printf "\n";
