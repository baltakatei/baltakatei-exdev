#!/bin/bash
# Desc:Marks the time when a remote server can be pinged.
# Depends: GNU Coreutils 8.32 (date, sleep), iputils-ping 3:20211215-1 (ping)
# Version: 0.0.1

delay=10;
domain="google.com"

if [[ -n "$1" ]]; then domain="$1"; fi;

printf "%s:STATUS:Starting ping...\n" "$(date -Is)";
while ! ping -c1 "$domain"; do
    sleep "$delay";
done;
printf "%s:Ping resolved.\n" "$(date -Is)";
exit 0;
