#!/usr/bin/env bash
# Desc: Create text files containing redirect wikicode: [[YYYY-mm-dd]] -> [[YYYY-mm#YYYY-mm-dd]]
# Usage: script.sh [year]
# Input: arg1: year (string)
# Output: none
# Version: 0.0.1

# Example: Running 'mw_create_redirect_day_month.sh 2022' creates
#   directory 'wikicode' in working directory. It then populates it
#   with text files with names ranging from '2022-01-01' through
#   '2022-12-31', taking leap years into account. Each text file
#   contains one line of wikicode redirecting a [[YYYY-mm-dd]] link to
#   [[YYYY-mm#YYYY-mm-dd]]. For example:
#
#   2022-02-28.txt:
#   ```
#   #REDIRECT [[2022-02#2022-02-28]]
#   ```
#
#   This is useful in combination with Mediawiki's
#   'maintenance/importTextFiles.php' script that imports text files
#   into a wiki, using each text file's name as a new page title; by
#   default it does not clobber (overwrite) pages if they already
#   exist.

yell() { echo "$0: $*" >&2; } # print script path and all args to stderr
die() { yell "$*"; exit 111; } # same as yell() but non-zero exit status
try() { "$@" || die "cannot $*"; } # runs args as command, reports args if command fails
list_days_of_year() {
    # Desc: Lists days of year in ISO-8601 YYYY-mm-dd format
    # Usage: script.sh [str year]
    # Input: arg1: year (4 digit gregorian calendar year)
    # Output: stdout: iso-8601 dates (newline delimited)
    # Depends: bash (5.1.16), GNU Coreutils (8.32; date, printf)
    # Version: 0.0.1
    # Ref/Attrib: [1] Linux Bash: How to print leading zeroes on a variable https://bytefreaks.net/gnulinux/bash/linux-bash-how-to-print-leading-zeroes-on-a-variable

    # Check arg
    ## Validate argument count
    if [[ $# -ne 1 ]]; then die "FATAL:Invalid number of arguments:$#"; fi;
    year="$1";

    ## Validate arg is a 4-digit year
    pattern='^[0-9]{4}$';    
    if [[ ! $year =~ $pattern ]]; then die "FATAL:Not a 4-digit year."; fi;
    if [[ ! $year -gt 1582 ]]; then die "FATAL:Not a gregorian calendar year."; fi;

    yyyy=$year;
    for (( month = 1; month <= 12; month++ )); do
        mm="$(printf %02d "$month")"; # two-digit month; See [1]
        #yell "DEBUG:mm:$mm";        
        for (( day = 1; day <= 31; day++ )); do
            dd="$(printf %02d "$day")"; # two-digit day; See [1]
            #yell "DEBUG:dd:$dd";            
            date_iso8601="$yyyy-$mm-$dd";
            if date --date="$date_iso8601" 1>/dev/random 2>&1; then
                printf "%s\n" "$date_iso8601";
            fi;
        done;
    done;
}; # iso-8601 dates (yyyy-mm-dd) to stdout
main() {
    dir_out="wikicode";
    year="$1";

    # check args
    ## check output directory
    if [[ ! -d $dir_out ]]; then mkdir "$dir_out"; fi;

    ## check arg count
    if [[ $# -ne 1 ]]; then die "FATAL:Invalid number of arguments:$#"; fi;
    
    ## check year
    pattern='^[0-9]{4}$';    
    if [[ ! $year =~ $pattern ]]; then die "FATAL:Not a 4-digit year."; fi;
    if [[ ! $year -gt 1583 ]]; then die "FATAL:Not a gregorian calendar year."; fi;

    # Cycle through each day of year
    while read -r yyyymmdd; do
        #yell "DEBUG:yyyymmdd:$yyyymmdd";
        ## Form wikicode redirect line
        wc_body="#REDIRECT [[${yyyymmdd%-*}#$yyyymmdd]]"; # e.g. "#REDIRECT [[2022-02#2022-02-28]"
        #yell "DEBUG:wc_body:$wc_body";

        ## Write wikicode to text file named with iso-8601 date (yyyy-mm-dd)
        path_out="$dir_out/$yyyymmdd";
        #yell "DEBUG:path_out:$path_out.txt";
        printf "%s\n" "$wc_body" > "$path_out";
    done < <(list_days_of_year "$year");
    
    return 0;
}; # main program

main "$@";
