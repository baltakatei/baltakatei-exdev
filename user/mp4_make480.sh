#!/bin/bash
# Desc: Reëncodes .mp4 videos in a specified directory.
# Usage: mp4_make480.sh [dir in] [dir out]
# Input: arg1  dir  input dir
#        arg2  dir  output dir (optional)
# Output: dir   ./out/
#         file  ./out/[files]_480.mp4
# Version: 0.1.2
# Depends: GNU parallel 20161222, ffmpeg 4.3.6-0
# Ref/Attrb: [1]: FFmpeg wiki: https://trac.ffmpeg.org/wiki/Scaling
declare -g dir_in dir_out;

check_input() {
    dir_in="$1";
    if [[ $# -le 0 ]]; then echo "FATAL:Insufficient args:$#" 1>&2; exit 1; fi;
    if [[ $# -eq 2 ]]; then
        dir_out="$2";
    else
        dir_out="./out_480";
    fi;
    export dir_out;
    if [[ ! -d "$dir_in" ]]; then echo "FATAL:Not a dir:$dir_in" 1>&2; exit 1; fi;
    mkdir -p "$dir_out";
    declare -p dir_in dir_out;  # debug
};
convert_video() {
    find "$dir_in" -mindepth 1 -maxdepth 1 -type f -iname "*.mp4" | \
        parallel job_ffmpeg "{}" "$path_out";
};
job_ffmpeg() {
    path_in="$1";
    path_out="$2";
    file_in="$(basename "$path_in")";
    path_out=./"$dir_out"/"${file_in%.mp4}"_480.mp4;
    opt_scale="scale=-2:480";  # See [1]
    declare -p path_in path_out file_in dir_out path_out opt_scale;
    ffmpeg -nostdin -i "$path_in" -vf "$opt_scale" "$path_out";
};
export -f job_ffmpeg;

main() {
    check_input "$@";
    convert_video;
};

main "$@";

# Author: Steven Baltakatei Sandoval
# License: GPLv3+
