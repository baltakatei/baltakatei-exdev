#!/usr/bin/env bash
# Desc: Calculates distances between two minecraft coordinates
# Depends: bash 5.1.16, bc 1.07.1
# Depends: BK-2020-03: yell(), die(), must()
# Version: 0.0.2
# Example: script.sh 0 0 0 1 1 1
#   Note: results in xyz distance of 1.73205…

yell() { echo "$0: $*" >&2; } # print script path and all args to stderr
die() { yell "$*"; exit 111; } # same as yell() but non-zero exit status
must() { "$@" || die "cannot $*"; } # runs args as command, reports args if command fails
checkFlt() {
    # Desc: Checks if arg is a float
    # Usage: checkFlt arg
    # Input: arg: float
    # Output: - return code 0 (if arg is float)
    #         - return code 1 (if arg is not float)
    # Example: if ! checkFlt $arg; then echo "not flt"; fi;
    # Version: 0.0.2
    # Depends: yell(), die(), bash 5.0.3
    # Ref/Attrib: JDB https://stackoverflow.com/a/12643073 float regex
    local returnState

    #===Process Arg===
    if [[ $# -ne 1 ]]; then
	die "ERROR:Invalid number of arguments:$#";
    fi;

    RETEST1='^[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)$'; # Regular Expression to test
    if [[ ! $1 =~ $RETEST1 ]] ; then
	returnState="false";
    else
	returnState="true";
    fi;

    #===Determine function return code===
    if [ "$returnState" = "true" ]; then
	return 0;
    else
	return 1;
    fi;
} # Checks if arg is float
main() {
    x1="$1"; y1="$2"; z1="$3";
    x2="$4"; y2="$5"; z2="$6";
    #declare -p x1 y1 z1 x2 y2 z2; # debug

    # Check inputs
    if ! checkFlt "$x1"; then die "FATAL:Not a float:$x1"; fi;
    if ! checkFlt "$y1"; then die "FATAL:Not a float:$y1"; fi;
    if ! checkFlt "$z1"; then die "FATAL:Not a float:$z1"; fi;
    if ! checkFlt "$x2"; then die "FATAL:Not a float:$x2"; fi;
    if ! checkFlt "$y2"; then die "FATAL:Not a float:$y2"; fi;
    if ! checkFlt "$z2"; then die "FATAL:Not a float:$z2"; fi;

    # Calculate distances
    dist_xyz_eq="sqrt( ($x2 - $x1)^2 + ($y2 - $y1)^2 + ($z2 - $x1)^2 )";
    dist_xyz="$(printf "%s\n" "$dist_xyz_eq" | bc -l;)";
    #declare -p dist_xyz_eq dist_xyz; # debug
    
    dist_xz_eq=" sqrt( ($x2 - $x1)^2 + ($z2 - $z1)^2 ) ";
    dist_xz="$(printf "%s\n" "$dist_xz_eq" | bc -l;)";
    declare -p dist_xz_eq dist_xz; # debug

    printf "dist_xyz:%s\n" "$dist_xyz";
    printf "dist_xz:%s\n" "$dist_xz";
}; # Main program

main "$@";
