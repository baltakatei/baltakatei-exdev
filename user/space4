#!/usr/bin/env bash
# Desc: Adds blank line every 4 lines
# Usage: cat file | space4
# Example: seq 1 10 | space 4
# Version: 0.0.1
# Depends: Bash 5.1.16

yell() { echo "$0: $*" >&2; } # print script path and all args to stderr
die() { yell "$*"; exit 111; } # same as yell() but non-zero exit status
must() { "$@" || die "cannot $*"; } # runs args as command, reports args if command fails
read_stdin() {
    # Desc: Consumes stdin; outputs as stdout lines
    # Input: stdin (consumes)
    # Output: stdout (newline delimited)
    # Example: printf "foo\nbar\n" | read_stdin
    # Depends: GNU bash (version 5.1.16)
    # Version: 0.0.1
    local input_stdin output;

    # Store stdin
    if [[ -p /dev/stdin ]]; then
        input_stdin="$(cat -)";
    fi; 
    
    # Store as output array elements
    ## Read in stdin
    if [[ -n $input_stdin ]]; then
        while read -r line; do
            output+=("$line");
        done < <(printf "%s\n" "$input_stdin");
    fi;

    # Print to stdout
    printf "%s\n" "${output[@]}";
}; # read stdin to stdout lines
main() {
    # Depends: BK-2020-03: yell(), die(), must(), read_stdin()
    if [[ $# -gt 0 ]]; then die "FATAL:This script uses no positional arguments.:$#"; fi;
    n=0;
    while read -r line; do
        printf "%s\n" "$line";
        if ! (( (n + 1) % 4 )); then
            printf "\n";
        fi;
        ((n++));
    done < <(read_stdin);
}; # main program

main "$@";

# Author: Steven Baltakatei Sandoval
# License: GPLv3+
