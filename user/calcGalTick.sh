#!/bin/bash
# Desc: Prints a list of galactic tick dates
# Usage: ./calcGalTick.sh [int start] [int end]
# Input: arg1: int   start index
#        arg2: int   end index
# Example: ./calcGalTick.sh 0 250
# Depends: bc 1.07.1; date (GNU coreutils) 8.32
# Version: 0.0.2
# Ref/Attrib: [1] Galactic Tick Day Calendar. (2021-10-20). https://github.com/GalacticTick/GalacticTick.github.io/blob/master/documents/press-3.pdf

# Galactic Tick duration equation (seconds)
## (Earth years per galactic turn) / (100ths of arcseconds per turn) * (solar days in Earth year)
sGalTickEq="(225 * 10^6) / (1296000 * 100) * (365.25) * (24*3600)";
#sGalTickEq="(225 * 10^6) / (1296000 * 100) * (365.24217) * (24*3600)";

# Galactic tick duration (seconds)
sGalTick="$(echo "$sGalTickEq" | bc -l)"; # 54788453.99999999999996457600
#sGalTick="$(printf "%.0f" "$sGalTick")"; # round to int

# Unix epoch of first tick (seconds; date Hans Lippershey patented the first telescope)
ueFirstTick="$(date --date="1608-10-02" +%s)";

yell() { echo "$0: $*" >&2; } # print script path and all args to stderr
die() { yell "$*"; exit 111; } # same as yell() but non-zero exit status
must() { "$@" || die "cannot $*"; } # runs args as command, reports args if command fails
printGalTickDate() {
    # Usage: printGalTickDate [int]
    # Input:  arg1: nthGalTick  int  n'th observance since frist Galactic Tick
    #         var: sGalTick:    int   Galactic tick duration
    #         var: ueFirstTick: int  Unix epoch of first tick
    # Output: str: ISO-8601 date string
    # Example: printGalTickDate 239
    #   2023-09-09
    # Depends: bc 1.07.1; date (GNU coreutils) 8.32
    # Version: 0.0.1
    local nthGalTick output;

    # Calc Unix epoch of n'th observance since first Galactic Tick
    ## 0th tick was first tick
    nthGalTick="$1";
    ueTickEq="( $ueFirstTick + ( ($nthGalTick - 0) * $sGalTick ) )";
    ueTick="$( echo "$ueTickEq" | bc -l )";

    # Print calendar date
    #output="$(TZ=UTC date --date="@$ueTick" +%Y-%m-%dT%H:%M:%S%z)";
    output="$(TZ=UTC date --date="@$ueTick" +%Y-%m-%d)";

    printf "%s\n" "$output";
}; # print ISO-8601 calendar date of tick
main() {
    start="$1";
    end="$2";
    for ((tick="$start"; tick<="$end"; tick++)); do
        printf "%03d: " "$tick"; # observance number
        printGalTickDate "$tick"; # date
    done;
}; # main program

main "$@";

# Author: Steven Baltakatei Sandoval
# License: GPLv3+
