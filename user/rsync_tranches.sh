#!/usr/bin/env bash
# Desc: A bash function for splitting up rsync jobs based on size

echo "ERROR:This is a bash function, not a script." 2>&1; exit 1;

function rsync_tranches() {
    # Desc: Runs rsync in parallel across different files size ranges
    # Example: rsync_tranches -avu --progress --dry-run ./SOURCE/ ./DEST/
    # Depends: rsync 3.2.7
    # Version: 0.1.1
    local -a rsync_opts=();
    local source dest;

    # Parse arguments until source and destination are found
    while [[ $# -gt 0 ]]; do
        case "$1" in
            # If it's not an option, assume it's the source, then the destination
            -*)
                rsync_opts+=("$1");
                shift;
                ;;
            *)
                ## If not a file or directory, assume option
                if [[ ! -f "$1" ]] && [[ ! -d "$1" ]]; then
                    rsync_opts+=("$1");
                    shift;
                fi;
                ## If valid file or directory, assume source or dest path
                if [[ -z "$source" ]]; then
                    source="$1";
                else
                    dest="$1";
                fi;
                shift;
                ;;
        esac;
    done;

    # Validate that source and destination are set
    if [[ -z "$source" ]] || [[ -z "$dest" ]]; then
        echo "Error: Source and destination directories must be specified." 1>&2;
        return 1;
    fi;

    # Tranche 1: 0 to 1MiB-1
    rsync --min-size='0' --max-size='1MiB-1' "${rsync_opts[@]}" "$source" "$dest" &
    sleep 2;

    # Tranche 2: 1MiB to 2MiB-1
    rsync --min-size='1MiB' --max-size='2MiB-1' "${rsync_opts[@]}" "$source" "$dest" &

    # Tranche 3: 2MiB to 4MiB-1
    rsync --min-size='2MiB' --max-size='4MiB-1' "${rsync_opts[@]}" "$source" "$dest" &

    # Tranche 4: 4MiB to 8MiB-1
    rsync --min-size='4MiB' --max-size='8MiB-1' "${rsync_opts[@]}" "$source" "$dest" &

    # Tranche 5: 8MiB to 16MiB-1
    rsync --min-size='8MiB' --max-size='16MiB-1' "${rsync_opts[@]}" "$source" "$dest" &

    # Tranche 6: 16MiB to 32MiB-1
    rsync --min-size='16MiB' --max-size='32MiB-1' "${rsync_opts[@]}" "$source" "$dest" &

    # Tranche 7: 32MiB to 64MiB-1
    rsync --min-size='32MiB' --max-size='64MiB-1' "${rsync_opts[@]}" "$source" "$dest" &

    # Tranche 8: 64MiB to 128MiB-1
    rsync --min-size='64MiB' --max-size='128MiB-1' "${rsync_opts[@]}" "$source" "$dest" &

    # Tranche 9: 128MiB to 256MiB-1
    rsync --min-size='128MiB' --max-size='256MiB-1' "${rsync_opts[@]}" "$source" "$dest" &

    # Tranche 10: 256MiB to 512MiB-1
    rsync --min-size='256MiB' --max-size='512MiB-1' "${rsync_opts[@]}" "$source" "$dest" &

    # Tranche 11: 512MiB to 1024MiB-1
    rsync --min-size='512MiB' --max-size='1024MiB-1' "${rsync_opts[@]}" "$source" "$dest" &

    # Tranche 12: Greater than 1024MiB
    rsync --min-size='1024MiB' --max-size='8192PiB-1' "${rsync_opts[@]}" "$source" "$dest" &

    wait # Wait for all rsync processes to complete
};
export -f rsync_tranches;

# Author: Steven Baltakatei Sandoval
# License: GPLv3+
