#!/bin/bash
# Desc: Encrypts a batch of files in the current working directory according to a find pattern.
# Usage: gpg_encrypt_batch.sh [PATH key] [PATTERN]
# Example: gpg_encrypt_batch.sh "/tmp/key.txt" "*.tar.xz"
# Depends: Bash 5.1.16, gpg (GnuPG) 2.2.27, libgcrypt 1.9.4
# Version: 0.0.1

PATH_KEY="$1"; # symmetric encryption passphrase is first line of this file
FIND_PATTERN="$2"; # adjust me (e.g. "*.tar.xz")

passphrase="$(head -n1 "$PATH_KEY")"; # first line

while read -r line; do
    printf "STATUS:Beginning encryption of:%s\n" "$line"; sleep 1;
    time gpg --symmetric --no-symkey-cache \
         --passphrase "$passphrase" \
         --pinentry-mode loopback \
         --cipher-algo AES256 --no-armor \
         --batch --yes \
         --output "$line".gpg \
         "$line" && \
        printf "STATUS:Finished encrypting:%s\n" "$line" 1>&2 &
done < <(find . -maxdepth 1 -type f -name "$FIND_PATTERN" | sort);
