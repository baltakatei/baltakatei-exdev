#!/bin/bash
#Desc:Lowercases portion of directory names preceding '..'
#Usage: lowerBeforeDoubleDotinDirName.sh
#Example case: If working directory contains directory named
#  '0xDEADBEEF..Human description', it is renamed
#  '0xdeadbeef..Human description'.

for dirSlash in ./*; do
    if [[ -d $dirSlash ]]; then
	echo "dirSlash:$dirSlash";
	dir="$(basename "$dirSlash")";
	echo "dir:$dir";
	first="${dir%..*}";
	echo "first:$first";
	second="${dir#*..}";
	echo "second:$second";
	firstNoCaps="$(echo "$first" | tr '[:upper:]' '[:lower:]')";
	echo "firstNoCaps:$firstNoCaps";
	newPath=./"$firstNoCaps".."$second";
	echo "newPath=$newPath";
	echo "===========================";
	mv "$dirSlash" "$newPath";
    fi;
done
