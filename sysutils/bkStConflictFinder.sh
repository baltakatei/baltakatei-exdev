#!/bin/bash
# Desc: Identifies `sync-conflict` files in specified directory
#   recursively.
# Usage: syncthingConflictFinder.sh arg1
# Example: syncthingConflictFinder.sh "$HOME/Sync/"
# Input: arg1   [path of dir to search recursively]
# Version: 0.1.0

arg1="$1"; # Get argument 1

# Validate dir
if [ -d "$arg1" ]; then
    searchDir="$arg1";
    echo "DEBUG:searchDir:$searchDir" 1>&2;
else
    echo "ERROR:Not a dir:$arg1" 1>&2; exit 1;
fi;

# List `sync-conflict` files.
find -L "$searchDir" -type f -iname "*sync-conflict*" -exec du -h {} \; 2>/dev/null;

# List `sync-conflict` files of size 1 MB to 999 GB.
#find -L "$searchDir" -type f -iname "*sync-conflict*" -exec du -h {} \; 2>/dev/null | grep "G      \|M     ";


#==License Info==
# Author: Steven Baltakatei Sandoval (reboil.com)
# License: GPLv3+

#===Dependency Version Information===
# find (GNU findutils) 4.6.0.225-235f
# Copyright (C) 2019 Free Software Foundation, Inc.
# License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>.
# This is free software: you are free to change and redistribute it.
# There is NO WARRANTY, to the extent permitted by law.

# Written by Eric B. Decker, James Youngman, and Kevin Dalley.
# Features enabled: D_TYPE O_NOFOLLOW(enabled) LEAF_OPTIMISATION FTS(FTS_CWDFD) CBO(level=2) 


# grep (GNU grep) 3.3
# Copyright (C) 2018 Free Software Foundation, Inc.
# License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>.
# This is free software: you are free to change and redistribute it.
# There is NO WARRANTY, to the extent permitted by law.

# Written by Mike Haertel and others; see
# <https://git.sv.gnu.org/cgit/grep.git/tree/AUTHORS>.
