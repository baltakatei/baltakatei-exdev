#!/usr/bin/env bash

yell() { echo "$0: $*" >&2; } # print script path and all args to stderr
die() { yell "$*"; exit 111; } # same as yell() but non-zero exit status
try() { "$@" || die "cannot $*"; } # runs args as command, reports args if command fails
list_days_of_year() {
    # Desc: Lists days of year in ISO-8601 YYYY-mm-dd format
    # Usage: script.sh [str year]
    # Input: arg1: year (4 digit gregorian calendar year)
    # Output: stdout: iso-8601 dates (newline delimited)
    # Depends: bash (5.1.16), GNU Coreutils (8.32; date, printf)
    # Version: 0.0.1
    # Ref/Attrib: [1] Linux Bash: How to print leading zeroes on a variable https://bytefreaks.net/gnulinux/bash/linux-bash-how-to-print-leading-zeroes-on-a-variable

    # Check arg
    ## Validate argument count
    if [[ $# -ne 1 ]]; then die "FATAL:Invalid number of arguments:$#"; fi;
    year="$1";

    ## Validate arg is a 4-digit year
    pattern='^[0-9]{4}$';    
    if [[ ! $year =~ $pattern ]]; then die "FATAL:Not a 4-digit year."; fi;
    if [[ ! $year -gt 1582 ]]; then die "FATAL:Not a gregorian calendar year."; fi;

    yyyy=$year;
    for (( month = 1; month <= 12; month++ )); do
        mm="$(printf %02d "$month")"; # two-digit month; See [1]
        #yell "DEBUG:mm:$mm";        
        for (( day = 1; day <= 31; day++ )); do
            dd="$(printf %02d "$day")"; # two-digit day; See [1]
            #yell "DEBUG:dd:$dd";            
            date_iso8601="$yyyy-$mm-$dd";
            if date --date="$date_iso8601" 1>/dev/random 2>&1; then
                printf "%s\n" "$date_iso8601";
            fi;
        done;
    done;
}; # iso-8601 dates (yyyy-mm-dd) to stdout

# Test
wc -l < <(list_days_of_year 1583); # should report 365 lines
wc -l < <(list_days_of_year 2023); # should report 365 lines
wc -l < <(list_days_of_year 2024); # should report 366 lines (leap year)
wc -l < <(list_days_of_year 2025); # should report 365 lines
wc -l < <(list_days_of_year 9999); # should report 365 lines

# Author: Steven Baltakatei Sandoval
# License: GPLv3+

# Ref/Attrib
# # date (GNU coreutils) 8.32
# # Copyright (C) 2020 Free Software Foundation, Inc.
# # License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>.
# # This is free software: you are free to change and redistribute it.
# # There is NO WARRANTY, to the extent permitted by law.

# # Written by David MacKenzie.


# # printf (GNU coreutils) 8.32
# # Copyright (C) 2020 Free Software Foundation, Inc.
# # License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>.
# # This is free software: you are free to change and redistribute it.
# # There is NO WARRANTY, to the extent permitted by law.

# # Written by David MacKenzie.


# # GNU bash, version 5.1.16(1)-release (x86_64-pc-linux-gnu)
# # Copyright (C) 2020 Free Software Foundation, Inc.
# # License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>

# # This is free software; you are free to change and redistribute it.
# # There is NO WARRANTY, to the extent permitted by law.
