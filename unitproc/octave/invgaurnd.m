## Copyright (C) 2012 Rik Wehbring
## Copyright (C) 1995-2016 Kurt Hornik
## Copyright (C) 2021 Steven Baltakatei Sandoval
##
## This program is free software: you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, see
## <http://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn  {} {} invgaurnd (@var{mu}, @var{lambda})
## @deftypefnx {} {} invgaurnd (@var{mu}, @var{lambda}, @var{r})
## @deftypefnx {} {} invgaurnd (@var{mu}, @var{lambda}, @var{r}, @var{c}, @dots{})
## @deftypefnx {} {} invgaurnd (@var{mu}, @var{lambda}, [@var{sz}])
## Return a matrix of random samples from the inverse gaussian distribution with
## parameters mean @var{mu} and shape parameter @var{lambda}.
##
## When called with a single size argument, return a square matrix with
## the dimension specified.  When called with more than one scalar argument the
## first two arguments are taken as the number of rows and columns and any
## further arguments specify additional matrix dimensions.  The size may also
## be specified with a vector of dimensions @var{sz}.
##
## If no size arguments are given then the result matrix is the common size of
## @var{mu} and @var{lambda}.
## @end deftypefn

## Author: Steven Sandoval <baltakatei@gmail.com>
## Description: Random variates from the inverse gaussian distribution

function rnd = invgaurnd (mu, lambda, varargin)

  if (nargin < 2)
    print_usage ();
  endif

  if (! isscalar (mu) || ! isscalar (lambda))
    [retval, mu, lambda] = common_size (mu, lambda);
    if (retval > 0)
      error ("invgaurnd: MU and LAMBDA must be of common size or scalars");
    endif
  endif

  if (iscomplex (mu) || iscomplex (lambda))
    error ("invgaurnd: MU and LAMBDA must not be complex");
  endif

  if (nargin == 2)
    sz = size (mu);
  elseif (nargin == 3)
    if (isscalar (varargin{1}) && varargin{1} >= 0)
      sz = [varargin{1}, varargin{1}];
    elseif (isrow (varargin{1}) && all (varargin{1} >= 0))
      sz = varargin{1};
    else
      error ("invgaurnd: dimension vector must be row vector of non-negative integers");
    endif
  elseif (nargin > 3)
    if (any (cellfun (@(x) (! isscalar (x) || x < 0), varargin)))
      error ("invgaurnd: dimensions must be non-negative integers");
    endif
    sz = [varargin{:}];
  endif

  if (! isscalar (mu) && ! isequal (size (mu), sz))
    error ("invgaurnd: MU and LAMBDA must be scalar or of size SZ");
  endif

  if (isa (mu, "single") || isa (lambda, "single"))
    cls = "single";
  else
    cls = "double";
  endif;

  # Convert mu and lambda from scalars into matrices since scalar multiplication used
  if isscalar (mu)
    mu = mu * ones(sz);
  endif;
  if isscalar (lambda)
    lambda = lambda * ones(sz);
  endif;

  # Generate random variates
  # Ref/Attrib: Michael, John R., Generating Random Variates Using
  #   Transformations with Multiple Roots. The American Statistician, May 1976,
  #   Vol. 30, No. 2. https://doi.org/10.2307/2683801
  nu = randn(sz,cls);
  y = nu .** 2;
  x1 = mu;
  x2 = mu .** 2 .* y ./ (2 .* lambda);
  x3 = (- mu ./ (2 .* lambda)) .* sqrt(4 .* mu .* lambda .* y + mu .** 2 .* y .** 2);
  x = x1 + x2 + x3;
  z = rand(sz,cls);
  valTest1 = (mu ./ (mu + x)); # calculate test 1 value
  valTest2 = (mu ./ (mu + x)); # calculate test 2 value
  posTest1 = find(z <= valTest1); # perform test 1, save positions where test 1 true
  posTest2 = find(z > valTest2); # perform test 2, save positions where test 2 true
  ## indposTest1 = transpose(posTest1) # debug: list positions
  ## indposTest2 = transpose(posTest2) # debug: list positions
  ## indTest1 = z <= valTest1 # debug: show test 1 truth table
  ## indTest2 = z > valTest2 # debug: show test 2 truth table
  rnd = NaN(sz); # Initialize return array
  rnd(posTest1) = x(posTest1); # populate return matrix with corresp. elements of x that satisfy test 1
  rnd(posTest2) = (mu(posTest2) .** 2 ./ x(posTest2)); # populate return matrix with corresp. elements of x that satisfy test 2
  k = ! isfinite (mu) | !(lambda >= 0) | !(lambda < Inf); # store position matrix indicating which parts of output are invalid based on
                                                          #   elements of the matrices: mu, lambda.
  rnd(k) = NaN; # mark invalid positions of output matrix with NaN

endfunction


%!assert (size (invgaurnd (1,2)), [1, 1])
%!assert (size (invgaurnd (ones (2,1), 2)), [2, 1])
%!assert (size (invgaurnd (ones (2,2), 2)), [2, 2])
%!assert (size (invgaurnd (1, 2*ones (2,1))), [2, 1])
%!assert (size (invgaurnd (1, 2*ones (2,2))), [2, 2])
%!assert (size (invgaurnd (1, 2, 3)), [3, 3])
%!assert (size (invgaurnd (1, 2, [4 1])), [4, 1])
%!assert (size (invgaurnd (1, 2, 4, 1)), [4, 1])

## Test class of input preserved
%!assert (class (invgaurnd (1, 2)), "double")
%!assert (class (invgaurnd (single (1), 2)), "single")
%!assert (class (invgaurnd (single ([1 1]), 2)), "single")
%!assert (class (invgaurnd (1, single (2))), "single")
%!assert (class (invgaurnd (1, single ([2 2]))), "single")

## Test input validation
%!error invgaurnd ()
%!error invgaurnd (1)
%!error invgaurnd (ones (3), ones (2))
%!error invgaurnd (ones (2), ones (3))
%!error invgaurnd (i, 2)
%!error invgaurnd (2, i)
%!error invgaurnd (1,2, -1)
%!error invgaurnd (1,2, ones (2))
%!error invgaurnd (1, 2, [2 -1 2])
%!error invgaurnd (1,2, 1, ones (2))
%!error invgaurnd (1,2, 1, -1)
%!error invgaurnd (ones (2,2), 2, 3)
%!error invgaurnd (ones (2,2), 2, [3, 2])
%!error invgaurnd (ones (2,2), 2, 2, 3)
